// Lab12Encryption.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <openssl/conf.h> // функции,структуры и константы настройки OpenSSL
#include <openssl/evp.h> // сами криптографические функции https://wiki.openssl.org/index.php/EVP
#include <openssl/err.h> // коды внутренних ошибок OpenSSL и их расшифровки
#include <openssl/aes.h>
#include <fstream>

// библиотеки OpenSSL (openssl.org) подключаются неявно динамически
// 1) компоновка *.lib на этапе сборки
// 2) старт *.dll на этапе запуска

// это значит,что сами функции в откомпилируемом виде находятся в dll
// ссылки на них - в *.lib а заголовки (или описания,прототипы)- в *.h в папках include




// для подключения готовых библиотек:
/*
1) В настройках проекта Каталоги VC++ ---> папки библиотек ->> прописать путь к .lib без названия файлов
2) В настройках проекта Компоновщик ---> ввод ---> Дополнительные зависимости - прописать названия файлов .lib
3) В настройках проекта Каталоги VC++ - Включаемые каталоги - прописать папку include





1) Создается указатель на несуществующую структуру
структура- тип данных в С++,близки к классу,различия минимальны

*/
using namespace std;
int main()
{
	// работа с криптофункциями OpenSSL:
	/*
	1) создание объекта с настройками
	2) шифрование
	3) финализация и вывод зашифрованных данных
	
	

	1) Создается указатель на несуществующую структуру
	структура- тип данных в С++,близки к классу,различия минимальны
	*/

	unsigned char *plaintext = (unsigned char *)" Some text Some text Some text Some text";// исходный текст
	int plaintext_len = strlen((char *)plaintext); // длина текста
	unsigned char *key = (unsigned char *)" 0123456789"; // пароль (ключ)
	unsigned char *iv = (unsigned char *)" 0123456789012345";// инициализирующий вектор, рандомайзер
	unsigned char cryptedtext[256]; // зашифрованный результат
	unsigned char decryptedtext[256]; // расшифрованный результат



	//1) Создается указатель на несуществующую структуру
	//структура - тип данных в С++, близки к классу, различия минимальны

	EVP_CIPHER_CTX *ctx; //структура

	// 2) Для указателя создается пустая структура настроек (метод,ключ,вектор инициализации и тд)

	ctx = EVP_CIPHER_CTX_new(); // создание структуры с настройками метода


	// 3) Структура EVP_CIPHER_CTX заполняется настройками

	EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv); // инициализация методом AES, ключом и вектором
	// ctx -ссылка на объект структуру,куда заносятся параметры
	// EVP_aes_256_cbc()- ссылка на шифрующее ядро AES 256 (функция с алгоритмом)
	// key- ключ,пароль
	// iv - рандомайзер (случайный вектор)

	// 4) САМ ПРОЦЕСС ШИФРОВАНИЯ - ФУНКЦИЯ EVP_EncryptUpdate
	int len;
	EVP_EncryptUpdate(ctx, // объект с настройками
		cryptedtext, // входной параметр: ссылка, куда помещать зашифрованные данные
		&len, // выходной параметр: длина полученного шифра
		plaintext, // входной параметр: что шифровать
		plaintext_len); // входной параметр : длина входных данных
	int cryptedtext_len = len;

	// 5. Финализация процесса шифрования
	// необходима, если последний блок заполнен данными не полностью
	EVP_EncryptFinal_ex(ctx, cryptedtext + len, &len);
	cryptedtext_len += len;

	// 6. Удаление структуры
	EVP_CIPHER_CTX_free(ctx);

	for (int i = 0; i < cryptedtext_len; i++)
	{
		cout << hex  << cryptedtext[i];
		if ((i + 1) % 32 == 0) cout <<  endl;
	}

	cout << endl;

	// РАСШИФРОВКА
	// 1.
	ctx = EVP_CIPHER_CTX_new(); // создание структуры с настройками метода

	// 2.
	EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv); // инициализация методом AES, ключом и вектором

	// 3.
	EVP_DecryptUpdate(ctx, decryptedtext, &len, cryptedtext, cryptedtext_len); // СОБСТВЕННО, ШИФРОВАНИЕ

	// 4.
	int dectypted_len = len;
	EVP_DecryptFinal_ex(ctx, decryptedtext + len, &len);

	// 5.
	dectypted_len += len;
	EVP_CIPHER_CTX_free(ctx);
	decryptedtext[dectypted_len] = '\0';
	cout << decryptedtext << endl;


	// Шифрование файлов
	// Создаются 2 файловых потока - временные буферы чтения записи, буфер для зашифрованных данных
	// Цикл While работающий пока в файле остались не прочитанные данные в цикле: считывание очередной порции из файла,шифрование порции
	// запись шифра в другой файл


	// 1)ШИФРОВАНИЕ ФАЙЛА

	// производится точно так же,но порциями в цикле
	fstream f0, f_enctypted, f_decrypted;
	f0.open("f0.txt", std::fstream::in | std::fstream::binary); // файл с исходными данными

										 // файл для зашифрованных данных
	f_enctypted.open("f_enctypted.txt",
		std::fstream::out | std::fstream::trunc | std::fstream::binary);

	char buffer[256] = { 0 };
	char out_buf[256] = { 0 };

	ctx = EVP_CIPHER_CTX_new();
	EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv); // инициализация методом AES, ключом и вектором
															   // ctx -ссылка на объект структуру,куда заносятся параметры
															   // EVP_aes_256_cbc()- ссылка на шифрующее ядро AES 256 (функция с алгоритмом)
															   // key- ключ,пароль
															   // iv - рандомайзер (случайный вектор)
	len = 0; // длина выходных данных
	f0.read(buffer, 256);
	while (f0.gcount() > 0) // цикл, пока из файла что-то считывается (пока размер считанной порции > 0)
	{
		// шифрование порции
		EVP_EncryptUpdate(ctx, // объект с настройками
			(unsigned char *)out_buf, // входной параметр: ссылка, куда помещать зашифрованные данные
			&len, // выходной параметр: длина полученного шифра
			(unsigned char *)buffer, // входной параметр: что шифровать
			f0.gcount()); // входной параметр : длина входных данных

						  // вывод зашифрованной порции в файл
		f_enctypted.write(out_buf, len);

		// считывание следующей порции
		f0.read(buffer, 256);
	}
	EVP_EncryptFinal_ex(ctx, (unsigned char *)out_buf, &len);

	f_enctypted.write(out_buf, len);

	f_enctypted.close();
	f0.close();
	


	// ДЗ
	// 2) РАСШИФРОВАНИЕ ФАЙЛА


	// файл для расшифрованных данных
	f_decrypted.open("f_decrypted.txt",
		std::fstream::out | std::fstream::trunc | std::fstream::binary);
	f_enctypted.open("f_enctypted.txt",
		std::fstream::in  | std::fstream::binary);

	char in_buffer[256] = { 0 };
	char outp_buf[256] = { 0 };

	ctx = EVP_CIPHER_CTX_new(); // создание структуры с настройками метода
	EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv); // инициализация методом AES, ключом и вектором
															   // ctx -ссылка на объект структуру,куда заносятся параметры
															   // EVP_aes_256_cbc()- ссылка на расшифрующее ядро AES 256 (функция с алгоритмом)
															   // key- ключ,пароль
															   // iv - рандомайзер (случайный вектор)
	len = 0; // длина выходных данных
	f_enctypted.read(in_buffer, 256);
	while (f_enctypted.gcount() > 0) // цикл, пока из файла что-то считывается (пока размер считанной порции > 0)
	{
		// расшифрование порции
		EVP_DecryptUpdate(ctx, // объект с настройками 
			(unsigned char *)outp_buf, // входной параметр: ссылка, куда помещать расшифрованные данные
			&len, // выходной параметр: длина полученного расшифра
			(unsigned char *)in_buffer, // входной параметр: что расшифровать
			f_enctypted.gcount()); // входной параметр : длина входных данных

						  // вывод расшифрованной порции в файл
		f_decrypted.write(outp_buf, len);

		// считывание следующей порции
		f_enctypted.read(in_buffer, 256);
	}
	EVP_DecryptFinal_ex(ctx, (unsigned char *)outp_buf, &len);

	f_decrypted.write(outp_buf, len);

	f_decrypted.close();
	f_enctypted.close();

	
	
	getchar();
    return 0;
}

