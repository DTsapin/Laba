// ConsoleApplication6.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
using namespace std;
/* ОБЪЕКТЫ И КЛАССЫ (ЛАБОРАТОРНАЯ РАБОТА 5)  */


// Объект-сущность,объединяющая подходящие к друг другу по смыслу переменные и функции
// Объект имеет тип данных (класс)

// ПРИМЕР
//класс-Оружие
// int-число зарядов,ёмкость магазина,число оставшихся зарядов
// double-начальная скорость,дальность,вес,калибр пули
// bool-otics
// bool reload

// функция-зарядка (число оставшихся зарядов -max)
// функция проиграть анимацию ()
// функция выстрел (вес -1 патрон,анимация выстрела,число оставшихся зарядов


// объявляется пользовательский тип данных-класс
// class Имя_класса {
// int-число оставшихся зарядов;
// int емкость магазина=30
//double дальность=1500
//double начальная скорость;
//double вес;
// bool-optics;
// bool reload;

// функция-зарядка (число оставшихся зарядов -max)
// функция проиграть анимацию ()
// функция выстрел (вес -1 патрон,анимация выстрела,число оставшихся зарядов

// }

// Имя класса weapon1;
// Имя класса * weapon2 * new_Имя класса();

// weapon1.емкость магазина =60;
/* weapon.выстрел(параметры);

weapon -ёмкость магазина =60;
weapon-выстрел(параметры);



конструктор и деструктор-специфические функции объекта,которые срабатывают
автоматически,соответственно при создании и удалении объекта

имя конструктора совпадает с именем класса,имя деструктора это ~Имя_класса()



Свойства и методы класса имеет три спецификатора доступа : private,public и protected
Свойства и методы public могут вызываться из вне объекта
Свойства и методы private могут вызываться только методами самого объекта
protected ведёт себя более сложным образом и будет описана в теме "Наследование"






*/




// Классы и объекты имеют смысл только для человека на уровне исходных кодов
// после компиляции в машинные коды ЭВМ имеет дело с отдельными переменными и функциями 

class Weapon_class
{
public:

	Weapon_class() // в конструкторе проводится инициализация свойств класса и прочие требуемые начальные действия
	{
		weight = 3.5;
		catriges = capacity;
		cout << "Weapon_class" << endl;
	}
private:
	int catriges;                  // число оставшихся зарядов
	int capacity = 30;             // магазин
public:
	double range = 1500; // дальность
	double velocity; // скорость
	double weight;  // вес
	bool optics;   // оптика
	bool loaded;

	void load() // Перезарядка
	{
		catriges = capacity;
	}

	void play_animation();
	void shot()
	{
		return;
	}



   ~Weapon_class()
{
	cout << "~Weapon_class()" << endl;
}
};
void Weapon_class::play_animation() // Метод определяемый вне класса,начиная с имя_класса
{
	return;
}

int main()
{

	Weapon_class weapon1; // если объект компактный и не может значительно менять свой размер во время жизненного цикла
	Weapon_class* weapon2 = new Weapon_class(); // если объект значительно меняет свой размер во время жизненного цикла

	delete weapon2;


	getchar();
    return 0;
}

// задание-1) создать класс для вектора и матрицы (ядром класса будет private массив,хранящей матрицу или вектор)
//2)создать 2 конструктора в каждом классе: 
//без параметров (инициализирующей матрицу класса нулями) и с параметром-массивом(private) (копирующей значения параметра в матрицу класса)
//3) реализовать написанные раннее функции как члены класса
//4) объявить класс в .h файле с соответствующем названием,а методы прописать в .cpp файле с тем же названием

// VectorMatrices.cpp-собрать там все определения функции
// VectorMatrices.h-оставить объявления функции