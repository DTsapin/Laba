// Lab 8 Exe.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

/*	СТАТИЧЕСКИ И ДИНАМИЧЕСКИ ПОДКЛЮЧАЕМЫЕ БИБЛИОТЕКИ	*/

// *.lib (англ. library) - файлы статически подключаемых библиотек
// *.dll (Dynamic Linked Library) - файлы динамически подключаемых библиотек

// *.a (англ.) - статические библиотеки в Linux
// *.so (англ. source object) - динамические библиотеки в Linux

// Библиотека *.lib или *.dll - файл, содержащий готовый машинный код для
// многократного повторного использования

// Код из статических библиотек помещается в *.exe в момент его сборки (после компиляции)
// и далее является частью самого *.exe

// если несколько *.exe используют один и тот же *.lib код дублируется в каждом *.exe

// Код из динамической библиотеки может использоваться одновременно многими приложенинями
// из одного и того же *.dll файла, загруженного в ОП

// если нескольког *.exe используют один и тот же *.dll, *.dll может быть единственным на машине
// и использоваться всеми

// по сути ОС представляет собой набор типовых наиболее часто используемых программами функций,
// собранных в динамические библиотеки: функции для работы с файлами, с сетью, с устройствами,
// функции безопасности, менеджмент процессов и т.д.
// прикладные приложения, работая на ОС, не нуждаются в собственном коде для таких тривиальных операций
// преимущества 1) снижение размера приложений 2) унификация 3) безопасность (варианты взаимодействия
// приложений с системными ресурсами: файлами, аппаратурой ограничены типовым и безопасным набором
// системных функций)

// огромное количество сторонних, в т.ч. и открытых бесплатных библиотек собирается и подключается
// именно в виде lib или dll (PoDoFo - для работы и криптографической подписи PDF, множество эталонных
// библиотек libjpeg, libpng для работы со сжатием изображений, криптографические библиотеки OpenSSL, OpenSSH)

/*
СТАТИЧЕСКОЕ
- Фрагменты из LIB при компиляции добавляются в EXE и далее переносятся с ним

ДИНАМИЧЕСКОЕ НЕЯВНОЕ
- Exe ставится в зависимость от DLL в момент компиляции
- Требуется подключение небольшой lib (объёмом гораздо меньше, чем при статическом),
содержащей адреса на функций в DLL
- DLL грузится в ОП в момент загрузки EXE, если DLL не находится, приложение выключается

ДИНАМИЧЕСКОЕ ЯВНОЕ
- Приложение грузит DLL в память с помощью специальной функции LoadLibrary()
- Перед вызовом функции приложение должно сначала найти её в DLL по строковому имени
с помощью функции прототип_функции GetProcAddress(HINSTANCE ссылка_на_на_загруженнуд_DLL,"имя_функции")
- Если DLL не найдена, приложение технически может продолжать работу, но без
функционала из DLL
*/



extern int fnc1(double p1, char p2); // прототип функции, с помощью которого линковщик (компоновщик)
// находит саму функцию в подключённом *.lib

// неявное связывание: dll грузятся в ОП при запуске exe
// в случае динамического связывания к exe всё равно подключается lib, но этот lib содержит не
// реализации самих функций, а только таблицу адресов, где их искать в dll
extern "C" __declspec(dllimport) int fnc2(double p1, char p2); // по этому объявлению компоновщик ищет
// адрес функции в lib-таблице адресов

#include <Windows.h> // для использования типа CALLBACK, HINSTANCE
// явное связывание - отдаётся специальная команда на подгрузку dll внутри приложения
typedef int(CALLBACK* FUNC2)(double, char); // прототип или объявление функции - инструкция для компилятора
// в каком порядке и какого размера передавать в стек параметры
// и какой ожидать выходной параметр

// __stdcall - компилятор передаёт параметры в стек по порядку: p1, p2, p3
// __cdecl - компилятор передаёт параметры в обратном порядке: p3, p2, p1
// CALLBACK - псевдоним для __stdcall

#include <iostream>

using namespace std;

int main()
{
	cout << "FROM LIB:\tfnc1(10, 5) = " << fnc1(10, 5)<< endl;
	cout << "FROM imlicit DLL:\tfnc2(10, 5) = " << fnc2(10, 5) << endl;

	// ЯВНОЕ ДИНАМИЧЕСКОЕ СВЯЗЫВАНИЕ С БИБЛИОТЕКОЙ

	// 1. Загрузка файла dll в ОП
	HINSTANCE hDLL;               // ссылка (указатель) на загруженную DLL, которая будет загружаться 
	hDLL = LoadLibrary(L"Lab8 Dll.dll"); // L означает использование 16-битных символов - только для x64
	cout << hDLL << endl; // проверка, загрузилась ли библиотека (0 если нет)

	// 2. Поиск в загруженоой DLL  фукнции по строковому имени
	FUNC2 fnc2explicit = (FUNC2)GetProcAddress(hDLL,"fnc2"); // поиск и возвращение указателя на функцию
	cout << fnc2explicit << endl; // проверка, нашлась ли функция (0 если нет)

	// 3. Вызов внешней функции
	cout << "FROM explicit DLL:\tfnc2(10, 5) = " << fnc2explicit(10.0, 5)  << endl;

	getchar();
    return 0;
}

/*
	ДЛЯ ПОДКЛЮЧЕНИЯ СТАТИЧЕСКОЙ БИБЛИОТЕКИ
	1. Сам отдельный проект библиотеки собирать в *.lib (настройки -> "Тип конфигурации")
	2. В настройках проекта "Компоновщик" -> "Ввод" добавить название файла библиотеки *.lib
	3. В "Папки VC++" добавить путь к библиотеке
	4. В исходниках exe объявить функцию со спецификатором extern

	ДЛЯ НЕЯВНОГО ПОДКЛЮЧЕНИЯ ДИНАМИЧЕСКОЙ БИБЛИОТЕКИ
	1. Сам отдельный проект библиотеки собирать в *.dll (настройки -> "Тип конфигурации")
	2. К объявлениям функции добавлять __declspec(dllexport)
	3. В настройках проекта *.exe "Компоновщик" -> "Ввод" добавить название файла библиотеки *.lib
	4. В "Папки VC++" добавить путь к паре библиотек (dll + lib с адресами)
	5. В исходниках exe объявить функцию со спецификатором __declspec(import)

	ДЛЯ ЯВНОГО ПОДКЛЮЧЕНИЯ ДИНАМИЧЕСКОЙ БИБЛИОТЕКИ
	1. Сам отдельный проект библиотеки собирать в *.dll (настройки -> "Тип конфигурации")
	2. К объявлениям функции добавлять extern "C" __declspec(dllexport)
	3. В исходниках exe объявить прототип функции через typedef
	4. Загрузит библиотеку с помощью LoadLibrary (проверить, загрузилась ли, через значение указателя)
	5. Найти в загруженном модуле функцию по строковому имени (проверить, загрузилась ли, через значение указателя)
*/

/*
	В данном занятии вызывали только отдельные функции
	Такая тема, как хранение целых объектов в DLL будет рассмотрена,
	если в семестре останется дополнительное время и по желанию студентов
*/

/*
	Вместо повторного объявления внешних функций в приложении
	в профессиональной среде использование одних и тех же заголовочных файлов
	и для построения приложения, и для построения библиотеки
*/