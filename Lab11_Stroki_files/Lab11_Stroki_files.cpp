// Lab11_Stroki_files.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <iostream>
#include <cstring> // strcat(), strlen(), strcmp()
#include <string> // для c++ класса string
#include <fstream> // Для работы с файлами через fstream, ifstream, ofstream
#include <bitset> // Для двоичного окончания записи в cout

/* ФАЙЛЫ И СТРОКИ */

using namespace std;
// строки старого C-типа представляют из себя всего лишь массив символов с нулевым окончанием:
char cstr1[] = "C - style string1";
char cstr2[] = { 'C', '-', 's' , 't', 'y','l','e', ' ', 's' ,'t','r','i','n','g', '2', '\0' };
// ФУНКЦИИ ДЛЯ РАБОТЫ С C-СТРОКАМИ

// вывод на печать printf(char[], , ,) и printf(*char, , ,)

// с помощью printf можно не просто печатать готовую строку
// но и подставлять в неё другие переменные, попутно преобразовывая их
// в символьный вид и применяя форматирование
// форматирование printf(";базовая %s строка %f";, параметр1, параметр2)

// позиции, куда будут подствляться параметры, обозначаются в базовой строке
// символом %;

// Значение символов форматирования берется из справки к функции http://www.cplusplus.com/reference/cstudio/printf
// значение некоторых из них

// %f - параметр подставляется на место %f, оформленное в виде числа с плавающей точкой
// %s - параметр подставляется на место %s как строка
// %d - параметр подставляется на место %d как целое число
// %e - параметр подставляется на место %e как число в экспоненциальном виде


// Обьединение (контактенация) строк strcat(*char, *char)
// Копирование из одной строки в другую strcopy()
// Сравнене strcmp()
// Длина строки strlen()
// Вставка строки в подстроку производится в несколько действий

// В стандартной библиотеке C++ массив символов инкапсулирован в класс std::string
// В которой также собраны часто используемые методы обработки
// И для которого перегружены операторы + (конкатенация), ввод, ввывод, и т.д.
// Еще одно отличие от строк С - длина строки теперь задается отдельным числом, а не 
// Нулевым окончанием

std::string cppstr1 = "C++ - style string 1 ";
std::string cppstr2 = "";

int main()
{
	printf(cstr1);

	printf("\n\n");

	printf(" Insert double: %f, \n"
		" insert long long: %d, \n" // целое число
		" insert string : %s, \n"
		" insert exponential : %e, \n" // мантисса - дробное число между &gt;=1 и &lt;2, умножаемое на 10^экспонента
		" insert double with pricision : %10.2f\n" // 10.2 - 10 пустых мест до запятой и 2 знака после
		"*ERROR insert long long as double: %10.2f\n" // ОШИБКА неверное приведение типов
		"*ERROR insert double as int: % 010d\n", // ОШИБКА неверное приведение типов
		1000.15,
		132456789,
		" stirng = )",
		1123456.1123456,
		1.1123456,
		(long long)10000,
		10.5);

	// Далее идут примеры работы со строками в C и эквивалентыми примерами из С++
	// Сравнение
	/*C*/ int i = strcmp(cstr1, cstr2);
	/*C*/printf_s("Result of strcmp(%s, %s) = %d\n", cstr1, cstr2, i);
	/*C++*/cout << "Instead of strcmp() use (cppstr1 == cppstr2) = "
		<< (cppstr1 == cppstr2) << endl << endl; // Для сравнения строк перегружен логический оператор "=="

												 // Длина строки
	/*C*/int lnght = strlen(cstr1);
	/*C*/printf_s("Result of strlen(%s) = %d\n", cstr1, lnght);
	/*C++*/cout << "Instead of strlen() use std::string.lenght():\t"
		<< cppstr1.length() << endl << endl; // Для определения длины - метод lenght() класса string

											 // копирование из одной строки в другую
	/*C*/char cstr3[255];
	/*C*/strcpy_s(cstr3, 255, cstr2); // Требуется использование более сложного безопасного аналога strcpy_s()
	/*C*/printf_s("Result of strcpy(%s, %s) = %d\n", cstr3, cstr2);
	/*C++*/cppstr2 = cppstr1; // Для копирования перегружен оператор "="
	/*C++*/cout << "Instead of strcpy() use operator =:\t"
		<< cppstr2 << endl << endl;

	// Обьединение строк
	/*C*/strcat_s(cstr3, 255, cstr1);
	/*C*/cout << "Result of strcat(cstr2, cstr1) = " << cstr3 << endl;
	/*C++*/cout << "Instead of strcat() use operator +:\t"
		<< (cppstr1 + cppstr2) << endl << endl; // для конкатенации перегружен "+"

												// Цикл работы с файлом заключается в 
												// 0. Проверить существует ли файл
												// 0.1 При работе в С++ - создать обьект для работы с файлом
												// 1. Открытие файла
												// 2. Работа с файлом ( чтение ,запись, поиск) 
												// 3. Закрытие файла

												// С ФС программа работает не на прямую, а через функции ОС
												// функция_программы(чтение/запись/открытие) -> функции ОС из системной dll (+доп. проверки, безопасность) ->
												// Доступ к диску ( барабан, головка, сектора )

												// Работа с файлами на С аналогична работе со строкам: указатель на файл  + группа функций
												// Более подробно на самообучение
												// https://www.tutorialspoint.com/cprogramming/c_file_io.htm

												// std::fstream - универсальный класс для ввода ( чтения из файла ) и вывода ( запись в файл)
												// std::ifstream - класс для ввода ( чтения из файла )
												// std::ofstream - класс для вывода ( запись в файл ) 
												// аналогично cout - потоку, предназначенному для вывода в консоль

	fstream fs; // 0. Создание обьекта
				// 1. Открытие файла
	fs.open("Some text file.txt", std::fstream::in | std::fstream::out | std::fstream::trunc);
	//			0x01                   0x02                0x10 = 16dec
	//			0b0001				0b0010				0b10000
	/*fs.open("Some text file.txt" -При открытии файла необходимо указать имя файла
	А также режим открытия: С перезаписью или без, для чтения/для записи/ оба режима и т.д.
	std::fstream::in - Добавляется режим вывода из файла
	|std::fstream::out - Добавляется режим записи из файла
	|std::fstream::app - append добавляется режим с записью в конец файла
	|std::fstream::trunc); - trunc = truncate - режим с перезаписью всего файла
	*/






	getchar();
	return 0;
}
// ВЫВОДЫ 
/*
- Строки в C - это просто массивы char
- Наиболее частые ошибки - несоответствие числа параметров printf и числа %,
Отсутсвие нулевого окончания, попытки доступа к чужой памяти ( неправильная работа с указателями и длинами )
*/
