// Lab9Overloadfunc.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

int fnc1(double p1, char p2)
{
	cout << "fnc1(double, char)" << endl;
	return p1 + p2;
}
/* Различие функций по одному выходному параметру не является перезагрузкой,
так как на месте вызова компилятору недостаточно информации, чтобы решить,
какую функцию вызвать

double fnc(double p1, char p2)
{
cout << "fnc1(double, char)" << endl;
return p1 + p2;
}
*/

int fnc1(int p1, short p2)
{
	cout << "fnc1(int, short)" << endl;
	return p1 + p2;
}
/* Для компялатора критерием является сигнатура функции - имя + последовательность аргументов + их типы
даже если аргументы имеют разные названия, но типы и последовательность одинаковые
компялтор считает такой случай не перезагрузкой, а повторным объявлением, что считается ошибкой;
int fnc1(int p1, short p2)
{
cout << "fnc1(double, char)" << endl;
return a - b;
}

*/
int fnc1(long long p1, long long p2)
{
	cout << "fnc1(long long, long long)" << endl;
	return p1 + p2;
}

class sample_class
{
public:
	/*sample_class();*/

	int property1;
	double property2;
	int fnc1(double p1, char p2)
	{
		cout << "sample_class::fnc1(double, char)" << endl;
		return p1 + p2;
	}

	int fnc1(int p1, short p2)
	{
		cout << "sample_class::fnc1(int, short)" << endl;
		return p1 + p2;
	}
	// бинарные операторы прописываются вне класса, так как результат не обязательно
	// записывается в один из операндов, а может записываться в
	// некий третий объект
	// Унарные(1 аргумент) и бинарные(2 аргумента) операторы перезагружаются по разному
	// Унарные - в самом классе ( получается новый метод класса )
	// Бинарные - как отдельную функцию

	// Результат записывается в тот же самый объект
	// Для которого вызван оператор, поэтому унарные стоит отнести
	// К методам класса
	sample_class & operator += (const sample_class & operand)
		// здесь const - "защита от забывчивости " - не дает изменить внутри метода операнд,
		// который по логике и не должен меняться
	{
		property1 = property1 + operand.property1;
		this->property2 = this->property2 + operand.property2; // c this - эффект не меняется ( ключевое слово - указатель на объект, который вызвал метод)
		return (*this);
	};

	/* Общий синтаксис перезагрузки операторов:

	тип operator символ_оператора ( тип имя_параметра , тип имя_параметра, ... )
	{

	}*/
	sample_class & operator -= (const sample_class & operand);

};

sample_class & sample_class::operator -= (const sample_class & operand)
{
	property1 = property1 + operand.property1;
	this->property2 = this->property2 + operand.property2; // c this - эффект не меняется ( ключевое слово - указатель на объект, который вызвал метод)
	return (*this);
};
/*возвр.тип   | имя функции | список параметров */
sample_class   operator +   (const sample_class & lhs, // left hand side
	const sample_class & rhs) // right hand side
{
	sample_class result; // Создать новый объект для записи результатов

						 // Собственно, сложение
	result.property1 = lhs.property1 + rhs.property1;
	result.property2 = lhs.property2 + rhs.property2;
	return result;
};

ostream & operator << (ostream & os, // левосторонний операнд, cout
	sample_class & rhs) // правостороний операнд, собственно, то, что выводится
{
	os << "sample_class:" << endl;
	os << "\tproperty1 = " << rhs.property1 << ";" << endl;
	os << "\tprorerty2 = " << rhs.property2 << ";" << endl;
	return os;
}




int main() // Функция main НЕ МОЖЕТ быть перегружена
{
	double a = 10.5;
	char b = 15;
	int c = 10000;
	short d = 101325;
	long long e = 1;

	fnc1(a, b); // компилятор сам определяет набор параметров, что нужно вызвать первую перегруженную функцию
	fnc1(e, e); // компилятор сам определяет набор параметров, что нужно вызвать 3-ую перегруженную функцию
	fnc1(c, d); //компилятор сам определяет набор параметров, что нужно вызвать 2-ую перегруженную функцию

	sample_class obj1, obj2;
	obj1.fnc1(a, b);
	obj1.fnc1(c, d);

	obj1.property1 = 11;
	obj1.property2 = 10.5;
	obj2.property1 = 111;
	obj2.property2 = 111.5;


	// obj1 = obj1 * obj2;  По умолчанию компилятор не знает, что делать при вызове оператора "*" для новых пользовательских типов

	/* Применение и проверка работы оператора "+=" */

	cout << "*** before use of += " << endl;
	cout << " obj1.property1 = " << obj1.property1 << endl;
	cout << "obj1.property2 = " << obj1.property2 << endl;
	obj1 += obj2;
	cout << "*** after use of += " << endl;
	cout << " obj1.property1 = " << obj1.property1 << endl;
	cout << "obj1.property2 = " << obj1.property2 << endl;

	cout << obj1;
	cout << obj1 + obj2;

	getchar();
	return 0;
}

/* Перегруженные функции имеют одинаковые имена на уровне исходных кодов, для компилятора и в бинарнике -
Это разные функции с разными адресами
-Компилятор принемает решение, какую из перегрузок вызывать на месте,
при вызове, по составу и типу параметров, которые передал программист
- Служебное слово operator
- Унарные операторы перезагружаются внутри класса в виде методов, бинарные -
Снаруже в виде отдельных функций
*/

/*

Задание
Для класса векторов и матриц определить
операторы "+" (х2 для векторов и для матриц),
"-" (х2 для векторов и для матриц
"*" (х3)
"<<" (х1 внутри базового класса - матрицы )
*/


