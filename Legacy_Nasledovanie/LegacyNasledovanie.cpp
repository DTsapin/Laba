// LegacyNasledovanie.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
using namespace std;
// ЛАБОРАТОРНАЯ РАБОТА (НАСЛЕДОВАНИЕ КЛАССОВ)

// для сокращения объема кода,для предотвращения необходимости реализации переписывания ранне реализованных методов
// и структур данных,новые классы в С++ можно создавать на основе других классов-МЕХАНИЗМ НАСЛЕДОВАНИЯ


// Синтаксис:
// class имя_нового класса : [тип_наследования] имя родительского класса1,
                             // [тип_наследования] имя родительского класса2

// Наследование-доступность свойств и методов базового класса внутри производного класса
// даже если они не объявлены в производном






class Weapon_class
{
public:

	Weapon_class() // в конструкторе проводится инициализация свойств класса и прочие требуемые начальные действия
	{
		weight = 3.5;
		catriges = capacity;
		cout << "Weapon_class" << endl;
	}
private:
	int catriges;                  // число оставшихся зарядов
	int capacity = 30;             // магазин
public:
	double range = 1500; // дальность
	double velocity; // скорость
	double weight;  // вес
	bool optics;   // оптика
	bool loaded;

	void load() // Перезарядка
	{
		catriges = capacity;
	}

	void play_animation();
	void shot()
	{
		return;
	}



	~Weapon_class()
	{
	cout << "~Weapon_class()" << endl;
	}
};
void Weapon_class::play_animation() // Метод определяемый вне класса,начиная с имя_класса
{
	return;
}

// Weapon_class С ПРОШЛОЙ ЛАБЫ



class assault_rifle : public Weapon_class
{
public: // влияет на видимость снаружи функции assalut_rifle
	assault_rifle()

		: Weapon_class() // в начале коструктора assalut_rifle вызывается конструктор базового класса
	{
		return;
	}
};

class soldier
{
public:

	soldier() {};

	int health;
};

// множественное наследование

class combat_unit : public Weapon_class, public soldier
{
public:
	combat_unit()
	{

	}
};

// ключевое слово this внутри класса означает указатель на самого себя

// внутри объявления класса у public private protected один смысл
// при объявлении наследования - другой

int main()
{
	Weapon_class weapon1; // если объект компактный и не может значительно менять свой размер во время жизненного цикла
	Weapon_class* weapon2 = new Weapon_class(); // если объект значительно меняет свой размер во время жизненного цикла

	assault_rifle obj1;
	obj1.play_animation(); // public свойства и методы заимствуются из базового класса

	// у дочернего класса нескольких родителей доступны все их свойства и методы
	combat_unit obj2;
	obj2.health = 100; // доступны члены первого родительского класса
	obj2.load(); // доступны члены второго родительского класса


    return 0;
}

/*
Базовый класс |  Режим наследования   |  Производный класс
              |                        
private       |    public               Недоступен
protected     |   ----->               protected
public        |                         public


private       |  protected              Недоступен
protected      |  ------>               protected
public         |                        protected
         

private       private                  Недоступен
protected     ------>                   private
public                                  private



*/