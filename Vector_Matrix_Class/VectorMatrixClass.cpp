// VectorMatrixClass.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include "Vector_Matrix_Class.h"
#include <conio.h>
#include <math.h>
using namespace std;

// МЕТОДЫ КЛАССА МАТРИЦА

//Заполение матрицы
void Matrix::Fill()
{

	for (int i = 0; i < q; i++)
	{
		for (int j = 0; j < w; j++)
		{
			cout << "Enter [" << i << "][" << j << "] element of array: ";
			cin >> arr[i][j];
		}
	}
}
//Конструктор
Matrix::Matrix(int a, int b)
{
	q = a; w = b;

	arr = new int*[q];

	for (int i = 0; i < q; i++)

		arr[i] = new int[w];
}

//Конструктор копирования
Matrix::Matrix(const Matrix& a)
{
	q = a.q; w = a.w;

	arr = new int*[q];

	for (int i = 0; i < q; i++)
	{
		arr[i] = new int[w];

		for (int j = 0; j < w; j++)

			arr[i][j] = a.arr[i][j];
	}
}









//  operator= 
Matrix& Matrix::operator=(const Matrix& a)
{
	if (this != &a)
	{
		for (int i = 0; i < q; i++)

			delete[]arr[i];

		delete[]arr;

		q = a.q; w = a.w;

		arr = new int*[q];

		for (int i = 0; i < q; i++)
		{
			arr[i] = new int[w];
			for (int j = 0; j < w; j++)

				arr[i][j] = a.arr[i][j];
		}
	}
	return *this;
}

//Перегрузка оператора + 
Matrix Matrix::operator+(const Matrix &rhs)
{

	int q1 = rhs.q;
	int w1 = rhs.w;
	if (q1 == w1 && w == q) // проверка размерности
	{
		Matrix result(q1, w1);

		for (int i = 0; i < q1; i++)

			for (int j = 0; j < w1; j++)

				result.arr[i][j] = arr[i][j] + rhs.arr[i][j];

		return result;

	}
	else
	{
		cout << "error" << endl;//сообщение об ошибке "размерности не равны"
	}
}



//Перегрузка оператора * 
Matrix Matrix::operator*(const Matrix &rhs)
{

	int q1 = rhs.q;
	int w1 = rhs.w;

	if (q1 == w1 && w == q) // проверка размерности
	{

		Matrix result(q1, w1);

		for (int i = 0; i < q1; i++)

			for (int j = 0; j < w1; j++)
			{
				result.arr[i][j] = 0;

				for (int k = 0; k < w1; k++)

					result.arr[i][j] = arr[i][k] * rhs.arr[k][j];
			}
		return result;


	}
	else
	{
		cout << "error" << endl;//сообщение об ошибке "размерности не равны"
	}
}



//Перегрузка <<
ostream &operator << (ostream &os, Matrix &temp)
{
	for (int i = 0; i < temp.q; i++)
	{
		for (int j = 0; j < temp.w; j++)

			os << temp.arr[i][j] << " ";
		os << endl;
	}
	return os;
}

// перегрузка квадратных скобок


int* Matrix::operator[] (int a)
{
	q = a;
	int* p = arr[q];
	return p;
}

int Matrix::operator[] (short int b)
{
	return ((*this)[w][b]);
}












// МЕТОДЫ КЛАССА ВЕКТОР

//определение конструктора с аргументами по умолчанию

//для задания начальных значений закрытых данных

vector::vector(int mx = 0, int my = 0, int mz = 0)

{
	x = mx; y = my; z = mz;

	cout << "Vector (" << x << "," << y << "," << z << ") created.\n";

}


void vector::assign(int mx, int my, int mz) // присваивание

{
	x = mx; y = my; z = mz;

}



void vector::show(void) // ввод вектора

{
	cout << x << ", "; cout << y << ", "; cout << z << "\n";

}



vector vector::operator=(vector t) // перегрузка оператора =

{
	x = t.x; y = t.y; z = t.z;

	return *this;

}

vector vector::operator+(vector t) // перегрузка оператора +

{
	vector temp;

	temp.x = x + t.x; temp.y = y + t.y; temp.z = z + t.z;

	return temp;
}

vector vector::operator*(vector t) // перегрузка оператора *
{
	vector temp;

	temp.x = x * t.x; temp.y = y * t.y; temp.z = z * t.z;

	return temp;
}






int main()
{


	vector a, b, c; // создаются три объекта класса Вектор

	cout << "******************************\n";

	a.assign(1, 2, 3);

	b.assign(4, 5, 6);

	cout << "Vector a: "; a.show();

	cout << "Vector b: "; b.show();

	c = a + b; cout << "Vector c: "; c.show();

	c = a * b; cout << "Vector c:"; c.show();

	c = b = a;

	cout << "Vector c: "; c.show();

	cout << "Vector b: "; b.show();







	Matrix c4(4, 4);

	// !!! TODO должно работать на автомат
	cout << c4;

	c4[1][2] = 11;

	cout << c4;

	// !!! TODO должно работать на автомат





	Matrix m1(3, 3);   // объекты класса Матрица

	m1.Fill();

	Matrix m2(3, 3);

	m2.Fill();

	Matrix m3;

	m3 = m2 + m1;

	cout << m3;



	Matrix c1(3, 3);

	c1.Fill();

	Matrix c2(3, 3);

	c2.Fill();

	Matrix c3;

	c3 = c2 * c1;

	cout << c3;

	system("pause");

	getchar();
	return 0;

}


